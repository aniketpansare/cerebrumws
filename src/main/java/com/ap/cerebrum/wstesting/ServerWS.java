package main.java.com.ap.cerebrum.wstesting;

import main.java.com.ap.cerebrum.ws.soap.HelloWorldWS;
import main.java.com.ap.cerebrum.ws.soap.impl.HelloWorldWSImpl;

import org.apache.cxf.jaxws.JaxWsServerFactoryBean;


public class ServerWS {

    protected ServerWS() throws Exception {
        HelloWorldWSImpl implementor = new HelloWorldWSImpl();
        JaxWsServerFactoryBean factory = new JaxWsServerFactoryBean();
        factory.setServiceClass(HelloWorldWS.class);
        factory.setAddress("http://localhost:9090/helloWorldWS");
        factory.setServiceBean(implementor);
        //factory.getInInterceptors().add(new LoggingInInterceptor());
        //factory.getOutInterceptors().add(new LoggingOutInterceptor());
        factory.create();
    }

    public static void main(String args[]) throws Exception {
        new ServerWS();
        System.out.println("Server ready...");

        Thread.sleep(5 * 60 * 1000);
        System.out.println("Server exiting");
        System.exit(0);
    }
}