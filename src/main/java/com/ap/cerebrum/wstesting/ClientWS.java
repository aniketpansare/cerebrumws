package main.java.com.ap.cerebrum.wstesting;

import main.java.com.ap.cerebrum.ws.soap.HelloWorldWS;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;


public final class ClientWS {

    private ClientWS() {
    } 

    public static void main(String args[]) throws Exception {
    	JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
    	//factory.getInInterceptors().add(new LoggingInInterceptor());
    //	factory.getOutInterceptors().add(new LoggingOutInterceptor());
    	factory.setServiceClass(HelloWorldWS.class);
    	factory.setAddress("http://localhost:9090/helloWorldWS");
    	HelloWorldWS ws = (HelloWorldWS) factory.create();
    	 
    	String reply = ws.sayHi("Aniket");
    	System.out.println("Server said: " + reply);
    	System.exit(0); 

    }

}