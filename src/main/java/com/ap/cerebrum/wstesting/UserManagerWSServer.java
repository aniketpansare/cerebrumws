
package main.java.com.ap.cerebrum.wstesting;

import javax.xml.ws.Endpoint;

/**
 * This class was generated by Apache CXF 3.0.0-milestone2
 * 2014-03-16T16:33:59.288-07:00
 * Generated source version: 3.0.0-milestone2
 * 
 */
 
public class UserManagerWSServer{

    protected UserManagerWSServer() throws Exception {
        System.out.println("Starting Server");
        Object implementor = new main.java.com.ap.cerebrum.ws.soap.impl.UserManagerWSImpl();
        String address = "http://localhost:9090/UserManagerWSImplPort";
        Endpoint.publish(address, implementor);
    }
    
    public static void main(String args[]) throws Exception { 
        new UserManagerWSServer();
        System.out.println("Server ready..."); 
        
        Thread.sleep(5 * 60 * 1000); 
        System.out.println("Server exiting");
        System.exit(0);
    }
}
 
 