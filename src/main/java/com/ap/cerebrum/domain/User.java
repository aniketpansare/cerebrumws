package main.java.com.ap.cerebrum.domain;

import javax.xml.bind.annotation.XmlRootElement;

import main.java.com.ap.cerebrum.domain.impl.UserImpl;


@XmlRootElement
public abstract class User {

	/****************************************************************
	 * Enums (User Type, Status, Type Filters, Status Filters
	 ****************************************************************/
	
	public enum Type {
		ADMIN,
		CUSTOMER,
		UNVALIDATED
	}
	
	public enum TypeFilter{
		/** Multiple Objects of TypeFiler calling parameterized private constructor.
		 * Just like multiple Singleton objects*/
		ALL(null,true),  //Select * from Users where type!=null
		ADMIN(Type.ADMIN,false), //select * from Users where type==ADMIN
		CUSTOMER(Type.CUSTOMER,false), //select * from Users where type==CUSTOMER
		UNVALIDATED(Type.UNVALIDATED,false), //select * from Users where type==UNVALIDATED
		VALIDATED(Type.UNVALIDATED,true), //select * from Users where type!=UNVALIDATED
		NOTADMIN(Type.ADMIN, true);//select * from Users where type!=ADMIN
		
		public final Type type;
		public final boolean negate;  // if(true){use "!="}else{use "=="} during query construction.
		
		private TypeFilter(Type type,boolean negate)
		{
			this.type=type;
			this.negate=negate;
		}
		
	}
	
	
	/***************************************************************
	 * Static create Factory Methods (Call constructors)
	 ****************************************************************/
	
	public static User create()
	{
		return new UserImpl();
	}
	
	public static User create(String name,String email,Type userType)
	{
		return new UserImpl(name,email,userType);
	}
	
	/******************************************************************
	 * Getter and Setter Methods Interface
	 ******************************************************************/
	
	public abstract void setName(String name);

	public abstract String getName();
	
	public abstract String getUserId();

	public abstract void setUserId(String userId);

	public abstract String getEmail();

	public abstract void setEmail(String email);

	public abstract Type getUserType();

	public abstract void setUserType(Type userType);

}
