package main.java.com.ap.cerebrum.domain.impl;

import java.io.Serializable;
import java.util.Random;

import javax.xml.bind.annotation.XmlRootElement;

import main.java.com.ap.cerebrum.domain.User;


@XmlRootElement
public class UserImpl extends User implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
	 * Assign Unique identifier.
	 */
	private String userId;
	private String name;
	private String email;
	private Type userType;
	
	/***********************************************************
	 * Constructors
	 *************************************************************/
	
	public UserImpl(String name,String email, Type userType) {
		this.userId = String.valueOf((new Random(46)).nextInt(100000));
		this.name=name;
		this.email=email;
		this.userType=userType;
	}
	
	public UserImpl(){
		
	}

		
	/*********************************************************
	 *  Getter and setter methods 
	 *********************************************************/

	/**
	 * @return the userId
	 */
	@Override
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	@Override
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	@Override
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the userType
	 */
	@Override
	public Type getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	@Override
	public void setUserType(Type userType) {
		this.userType = userType;
	}


}
