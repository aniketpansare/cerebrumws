/**
 * 
 */
package main.java.com.ap.cerebrum.ws.soap;

import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import main.java.com.ap.cerebrum.IntegerUserMapAdapter;
import main.java.com.ap.cerebrum.domain.User;


/**
 * @author aniket
 *
 */
@WebService
@SOAPBinding(style=Style.DOCUMENT,use=Use.LITERAL)//optional
public interface HelloWorldWS {

	/**
	 * SayHi web service which says "Hello text".
	 * To ensure that parameter is named correctly in the xml use @WebParam(name="text")
	 * @param text
	 * @return
	 */
	@WebMethod
	String sayHi(@WebParam(name="text")String text);
	
	/**
	 * JAXB (Java XML Binding does not support Maps.
	 * Need XML adapter to map  the Map in to beans.
	 * JAXB is used to access or process XML data without needing to know XML.
	 * @return
	 */
	@WebMethod
	@XmlJavaTypeAdapter(IntegerUserMapAdapter.class)
	Map<Integer,User> getUsers();
	
}
