package main.java.com.ap.cerebrum.ws.soap.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import main.java.com.ap.cerebrum.IntegerUserMapAdapter;
import main.java.com.ap.cerebrum.domain.User;
import main.java.com.ap.cerebrum.ws.soap.HelloWorldWS;


//This annotation allows CXF to know which interface we want to create WSDL with.(HelloWorldWS interface)
@WebService(endpointInterface="main.java.com.ap.cerebrum.ws.soap.HelloWorldWS",
				serviceName="HelloWorldWS")
public class HelloWorldWSImpl implements HelloWorldWS {

	Map<Integer,User> users = new LinkedHashMap<Integer,User>();
	
	@Override
	public String sayHi(@WebParam(name = "text") String text) {
		return "Hello "+text;
	}

	@Override
	@XmlJavaTypeAdapter(IntegerUserMapAdapter.class)
	public Map<Integer, User> getUsers() {
		//fetch users in Map.
		return users;
	}

}
