
package main.java.com.ap.cerebrum.ws.soap.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class was generated by Apache CXF 3.0.0-milestone2
 * Sun Mar 16 16:33:59 MST 2014
 * Generated source version: 3.0.0-milestone2
 */

@XmlRootElement(name = "getUserByNameResponse", namespace = "http://restless.ws.cerebrum.ap.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserByNameResponse", namespace = "http://restless.ws.cerebrum.ap.com/")

public class GetUserByNameResponse {

    @XmlElement(name = "return")
    private main.java.com.ap.cerebrum.domain.User _return;

    public main.java.com.ap.cerebrum.domain.User getReturn() {
        return this._return;
    }

    public void setReturn(main.java.com.ap.cerebrum.domain.User new_return)  {
        this._return = new_return;
    }

}

