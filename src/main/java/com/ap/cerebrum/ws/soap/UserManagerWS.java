package main.java.com.ap.cerebrum.ws.soap;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import main.java.com.ap.cerebrum.domain.User;




@WebService(name = "UserManagerWS", targetNamespace = "http://restless.ws.cerebrum.ap.com/")
public interface UserManagerWS {

	@WebMethod(operationName = "createUser", action = "urn:CreateUser")
	public void createUser();
	@WebMethod(operationName = "getAllUsers", action = "urn:GetAllUsers")
	public List<User> getAllUsers(@WebParam(name = "arg0") User.TypeFilter typeFilter);
	@WebMethod(operationName = "getUser", action = "urn:GetUser")
	public User getUser(@WebParam(name = "arg0") String userId);
	@WebMethod(operationName = "getUserByName", action = "urn:GetUserByName")
	public User getUserByName(@WebParam(name = "arg0") String Name);
	@WebMethod(operationName = "authenticateUser", action = "urn:AuthenticateUser")
	public boolean authenticateUser(@WebParam(name = "arg0") String username,@WebParam(name = "arg1") String Password);
	
	
}
