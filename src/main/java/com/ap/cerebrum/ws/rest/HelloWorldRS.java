package main.java.com.ap.cerebrum.ws.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//Sets path to BASE_URL + /helloworld --> (Helloworld is a Resource)
@Path("/helloworld")
public class HelloWorldRS {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getPlainTextHi()
	{
		return "Hello Aniket";
	}

	@GET
	@Produces(MediaType.TEXT_XML)
	public String sayXMLHello()
	{
		return "<?xml version=\"1.0\"?>"+"<hello> Hello Aniket"+"</hello>";
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHTMLHello()
	{
		return "<html> <title>"+"Hello Aniket"+"</title>"+ 
				"<body><h1>" + "Hello Aniket" + "</body></h1>" + "</html> ";
	}
	
}
