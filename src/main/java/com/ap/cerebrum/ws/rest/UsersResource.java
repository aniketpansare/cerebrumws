package main.java.com.ap.cerebrum.ws.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import main.java.com.ap.cerebrum.domain.User;


@Path("/users")
public class UsersResource {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	/**************************************************************
	 * REST Method to return unique resource
	 *************************************************************/
	
	//Fetch single user resource
	@Path("{user}")
	public UserResource getTodo(@PathParam("user") String id) {
		return new UserResource(uriInfo, request, id);
	}
	
	/***************************************************************
	 * REST Methods (Return Collection of resources)
	 ******************************************************************/
	
	//Browser Integration
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<User> getUsersBrowser()
	{
		java.util.List<User> users = new ArrayList<User>();
		//use content provider and return all Users.
		users.add(User.create("Aniket","aniketpansare@gmail.com",User.Type.CUSTOMER));
		users.add(User.create("AP","ap@gmail.com",User.Type.UNVALIDATED));
		return users;
	}
	
	
	//Application Integration
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public List<User> getUsersApplication()
	{
		java.util.List<User> users = new ArrayList<User>();
		//use content provider and return all Users.
		users.add(User.create("Aniket","aniketpansare@gmail.com",User.Type.CUSTOMER));
		users.add(User.create("AP","ap@gmail.com",User.Type.UNVALIDATED));
		return users;
	}
	
	//Get Count
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getUserCount()
	{
		java.util.List<User> users = new ArrayList<User>();
		//use content provider and return all Users.
		users.add(User.create("Aniket","aniketpansare@gmail.com",User.Type.CUSTOMER));
		users.add(User.create("AP","ap@gmail.com",User.Type.UNVALIDATED));
		return String.valueOf(users.size());
	}
	
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void createUser(@FormParam("name")String name,
			@FormParam("email") String email,
			@FormParam("userType") String userType,
			@Context HttpServletResponse servletResponse) throws IOException
	{
		User.create(name,email,User.Type.valueOf(userType));
		//store user in database.
		servletResponse.sendRedirect("../SampleFormsREST/create_user.html");
	}
	
}
