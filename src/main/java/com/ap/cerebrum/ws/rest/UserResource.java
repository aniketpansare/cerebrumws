package main.java.com.ap.cerebrum.ws.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import main.java.com.ap.cerebrum.domain.User;


public class UserResource {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	//Parameter to identify unique user Object
	String userId; //use it in below retrieval method to return unique resource.
	
	//Constructor
	public UserResource(UriInfo uriInfo,Request request,String userId)
	{
		this.uriInfo=uriInfo;
		this.request=request;
		this.userId=userId;
	}
	
	/*****************************************************************
	 * REST METHODS
	 ******************************************************************/
	
	
	//For Browser Integration
	@GET
	@Produces(MediaType.TEXT_XML)
	public User getUserHTML()
	{
		User user = User.create("Aniket","aniketpansare@gmail.com",User.Type.CUSTOMER); //Replace with a content Provider.
		if(user==null)
			throw new RuntimeException("Get: User with id-"+userId+" Not Found." );
		return user;
	}
	
	//For Application Integration
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public User getUser()
	{
		User user = User.create("Aniket","aniketpansare@gmail.com",User.Type.CUSTOMER); //Replace with a content Provider.
		if(user==null)
			throw new RuntimeException("Get: User with id-"+userId+" Not Found." );
		return user;
	}
	
}
